import pygame

class Player():
    def __init__(self, surface, (x, y), colour=(128,0,128), radius=5, thickness=1):
        self.surface = surface
        self.x = x
        self.y = y
        self.radius = radius
        self.colour = colour
        self.thickness = thickness
        self.distance = 0

    def display(self):
        pygame.draw.circle(self.surface, self.colour, (int(self.x), int(self.y)), self.radius)

    def moveUp(self, y):
        self.y -= y

    def moveDown(self, y):
        self.y += y

    def moveRight(self, x):
        self.x += x

    def moveLeft(self, x):
        self.x -= x

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getRadius(self):
        return self.radius

    def setDistance(self, distance):
        self.distance = distance

    def getDistance(self):
        return self.distance

