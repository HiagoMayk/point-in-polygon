import pygame, time

pygame.mixer.init()

def play_music(path):
	pygame.mixer.music.load(path)
	pygame.mixer.music.play(-1)
	time.sleep(2)

def play_sound(path):
	song = pygame.mixer.Sound(path)
	song.play()
	time.sleep(2)

def wait_sound_and_music():
	while pygame.mixer.music.get_busy() == True:
		continue
# Exemplo de uso
#play_music("Audios\\Intro\\musica.mp3")
play_sound("Audios/Intro/Example.ogg")
wait_sound_and_music()
#
# "Audios\\Intro\\musica.mp3"
# "Audios\\Intro\\Example.ogg"
# "Audios\\Intro\\ACDC.ogg"
#
