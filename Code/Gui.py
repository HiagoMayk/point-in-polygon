import pygame
import random
from Player import *
from AudioManeger import *
import time, sys
from threading import Thread
import math

#http://www.petercollingridge.co.uk/pygame-physics-simulation/drawing-circles

## To audio devices
#http://python-sounddevice.readthedocs.io/en/latest/#sounddevice.query_devices

class Gui:

    def __init__(self, background_colour=(0,0,0), width=800, height=450):
        pygame.init()
        self.width = width
        self.height = height
        self.colors = {'red':(255,0,0), 'lime':(0,255,0), 'blue':(0,0,255), 'yellow':(255,255,0), 'cyan':(0,255,255), 'green':(0,128,0), 'marroon':(128,0,0), 'olive':(128,128,0), 'purple':(128,0,128), 'teal':(0,128,128), 'navy':(0,0,128), 'margenta':(255,0,255)}
        self.screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Point In Polygon') # Insert titler in the screen
        self.screen.fill(background_colour)
        self.players = []
        self.figure_rect = []
        self.state = "InitialMenu" # Game state
        self.option_initial = 0
        self.match_surface = pygame.Surface((self.width, self.height-(self.height/10.0)))
        self.audio_maneger = AudioManeger()
        self.music = [] # sound music in background
        self.effect = [] # sound effetc
        self.current_figure = [] # Current figure values Circle: (Tipe, x, y, radius), Rectangle: (tipe, x, y, wigth, heigth)
        self.thread_time = []
        self.time = 60 # match time
        self.sentinel_time = True
        pygame.font.init() # Init fonts
        self.font_default = pygame.font.get_default_font()
        self.text_time = pygame.font.SysFont(self.font_default, 45)
        self.clock = pygame.time.Clock() # Define atualization time of screen
        self.running = True
        self.multiplayer_channel0 = [] # Volume in differents sides in multiplayer match
        self.multiplayer_channe1 = []
        self.winner0 = False
        self.winner1 = False

    ## NOT USED: It is to get to clise window
    def globalEvents(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT: # exit event
                    sys.exit()

                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    sys.exit()

    def menus(self):
        #thread_events = Thread(target = self.globalEvents, args = ([])) # Global events threads
        #thread_events.start()
        while self.running:
            #for event in pygame.event.get():
             #   if event.type == pygame.QUIT:
              #      self.running = False

            if self.state == "Initial": # While the game have a pygame event
                self.initInitial()
                pygame.mixer.music.load("Audios/Voice/Bem_vindo.mp3")
                pygame.mixer.music.play()
                time.sleep(3)
                pygame.mixer.music.load("Audios/Voice/Aventura.mp3")
                pygame.mixer.music.play()
                time.sleep(4)
                self.state = "Tutorial"

            if self.state == "Tutorial":
                pygame.mixer.music.load("Audios/Voice/Deseja_ouvir.mp3")
                pygame.mixer.music.play()
                time.sleep(3)
                pygame.mixer.music.load("Audios/Voice/Se_sim.mp3")
                pygame.mixer.music.play()
                pygame.mixer.music.play()
                time.sleep(3)
                pygame.mixer.music.load("Audios/Voice/Se_nao.mp3")
                pygame.mixer.music.play()
                pygame.mixer.music.play()
                time.sleep(3)
                self.initTurorial()

            if self.state == "InitialMenu":
                pygame.mixer.music.load("Audios/Voice/Menu_modo_de_jogo.mp3")
                pygame.mixer.music.play()
                time.sleep(2)
                self.initInitialMenu()

            if self.state == "Singleplayer":
                sound = pygame.mixer.Sound("Audios/Match/match_floating_cities.ogg")
                self.multiplayer_channel0 = sound.play()
                self.initSingleplayerMatch()

            if self.state == "Multiplayer":
                sound = pygame.mixer.Sound("Audios/Match/match_floating_cities.ogg")
                self.multiplayer_channel0 = sound.play()
                self.multiplayer_channel1 = sound.play()
                #pygame.mixer.music.load("Audios/Match/match_floating_cities.mp3")
                #pygame.mixer.music.play(-1)
                self.initMultiplayerMatch()

            if self.state == "ScoreSingleplayer":
                pygame.mixer.music.stop()
                self.scoreSingleplayer()

            if self.state == "ScoreMultiplayer":
                pygame.mixer.music.stop()
                self.scoreMultiplayer()

            self.clock.tick(30)
            pygame.display.flip()
            pygame.display.update()

    def initInitial(self):
        initial_surface = pygame.Surface((self.width, self.height))
        initial_surface.fill((0,0,0))
        self.screen.blit(initial_surface, [0, 0])

        text_initial = pygame.font.SysFont(self.font_default, self.height/5)
        text = text_initial.render("Welcome to", 1, (255, 255, 255))
        self.screen.blit(text, (self.width/3, 30))

        text = text_initial.render("Point In Polygon", 1, (255, 255, 255))
        self.screen.blit(text, (self.width/4, 100))

        text_initial_subscript = pygame.font.SysFont(self.font_default, self.height/10)
        text = text_initial_subscript.render("A game for blind people", 1, (255, 255, 255))
        self.screen.blit(text, (self.width/3, 180))

        pygame.display.flip()
        pygame.display.update()

    def initTurorial(self):
        running = True
        while(running):
            for event in pygame.event.get():
                #if event.type == pygame.QUIT: # exit event
                 #   self.running = False
                  #  running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.option_initial = 1
                        pygame.mixer.music.load("Audios/Voice/Sim.mp3")
                        pygame.mixer.music.play()
                        #time.sleep(1)

                    if event.key == pygame.K_DOWN:
                        self.option_initial = 2
                        pygame.mixer.music.load("Audios/Voice/Nao.mp3")
                        pygame.mixer.music.play()
                        #time.sleep(1)

                    if event.key == pygame.K_RETURN and self.option_initial == 1:
                        self.state = "Tutorial"
                        running = False # leave this while"
                        pygame.mixer.music.load("Audios/Voice/Tutorial.mp3")
                        pygame.mixer.music.play()
                        time.sleep(40)

                    if event.key == pygame.K_RETURN and self.option_initial == 2:
                        self.state = "InitialMenu"
                        running = False

    def initInitialMenu(self):
        running = True
        pygame.mixer.music.load("Audios/Intro/intro_kings_of_tara.mp3")
        pygame.mixer.music.set_volume(0.4)
        pygame.mixer.music.play(-1)
        while(running):
            self.clock.tick(30)

            initial_surface = pygame.Surface((self.width, self.height))
            initial_surface.fill((0,0,0))
            self.screen.blit(initial_surface, [0, 0])

            text_initial = pygame.font.SysFont(self.font_default, self.height/5)
            text = text_initial.render("Welcome to", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3, 30))

            text = text_initial.render("Point In Polygon", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/4, 100))

            text_initial_subscript = pygame.font.SysFont(self.font_default, self.height/10)
            text = text_initial_subscript.render("A game for blind people", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3, 180))

            text_initial_menu = pygame.font.SysFont(self.font_default, self.height/10)
            text = text_initial_menu.render("Game Mode:", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.5, 280))

            text_initial_menu = pygame.font.SysFont(self.font_default, self.height/12)
            text = text_initial_menu.render("1 - Singleplayer", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.4, 320))

            text = text_initial_menu.render("2 - Multiplayer", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.4, 360))

            text = text_initial_menu.render("=>", 1, (255, 255, 255)) # Arrow. change to a arrow firure aften.

            if self.option_initial == 1:
                self.screen.blit(text, ((self.width/2.4)-35, 320 - 2))
            if self.option_initial == 2:
                self.screen.blit(text, ((self.width/2.4)-35, 360 - 2))

            for event in pygame.event.get():
                #if event.type == pygame.QUIT: # exit event
                 #   self.running = False
                  #  running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT and self.option_initial == 1:
                        #self.effect.stop()
                        self.effect = pygame.mixer.Sound("Audios/Voice/Descricao_singleplayer.ogg") #Voice: Singleplayer
                        self.effect.play()

                    if event.key == pygame.K_RIGHT and self.option_initial == 2:
                        #self.effect.stop()
                        self.effect = pygame.mixer.Sound("Audios/Voice/Descricao_multiplayer.ogg") #Voice: Singleplayer
                        self.effect.play()

                    if event.key == pygame.K_UP:
                        self.option_initial = 1
                        #self.effect.stop()
                        self.effect = pygame.mixer.Sound("Audios/Voice/Singleplayer.ogg") #Voice: Singleplayer
                        self.effect.play()
                        #time.sleep(1)

                    if event.key == pygame.K_DOWN:
                        self.option_initial = 2
                        #self.effect.stop()
                        self.effect = pygame.mixer.Sound("Audios/Voice/Multiplayer.ogg") # Voice: Multiplayer
                        self.effect.play()
                        #time.sleep(1)

                    if event.key == pygame.K_RETURN and self.option_initial == 1:
                        self.current_figure = self.generateRandomFigure()
                        pygame.mixer.music.stop()
                        self.effect.stop()
                        running = False # leave this while"
                        self.state = "Singleplayer"
                        self.screen.fill((0,0,0)) # Set black in screen background
                        self.createPoints() # Init points of players

                    if event.key == pygame.K_RETURN and self.option_initial == 2:
                        self.current_figure = self.generateRandomFigure()
                        pygame.mixer.music.stop()
                        self.effect.stop()
                        running = False # leave this while
                        self.state = "Multiplayer"
                        self.screen.fill((0,0,0)) # Set black in screen background
                        self.createPoints(n=2) # Init points of players

            pygame.display.flip()
            pygame.display.update()

    ## Execute in thead to count the match time. Only incresce time.
    def matchTime(self):
        while self.sentinel_time:
            time.sleep(1)
            self.time-=1

    def initSingleplayerMatch(self):
        time_surface = pygame.Surface((self.width, self.height-(self.height-(self.height/10))))
        running = True

        self.thread_time = Thread(target = self.matchTime, args = ([])) # Thread time configuration
        self.time = 60
        self.sentinel_time = True
        self.thread_time.start()

        while(running):
            self.clock.tick(30)

            if self.time == 0: # Max macth time
                running = False
                self.state = "ScoreSingleplayer"
                self.winner0 = False
                self.sentinel_time = False
                #self.time  = 60

            time_surface.fill((0,0,0)) # Set time in screen surface
            self.screen.blit(time_surface, [0, 0])

            text = self.text_time.render("Time: "+ str(self.time), 1, (255, 255, 255))
            self.screen.blit(text, (300, 2))

            for event in pygame.event.get():
                #if event.type == pygame.QUIT: # exit event
                 #   self.running = False
                  #  running = False

                if event.type == pygame.KEYDOWN:
                    ## Event to player 1
                    if event.key == pygame.K_LEFT and self.players[0].getX() <= 5:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_LEFT:
                        self.players[0].moveLeft(10)

                    elif event.key == pygame.K_RIGHT and self.players[0].getX() >= self.width-10:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()


                    elif event.key == pygame.K_RIGHT:
                        self.players[0].moveRight(10)

                    elif event.key == pygame.K_UP and self.players[0].getY() <= (self.height/10):
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_UP:
                        self.players[0].moveUp(10)

                    elif event.key == pygame.K_DOWN and self.players[0].getY() >= self.height-55:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_DOWN:
                        self.players[0].moveDown(10)

                self.match_surface.fill((0,0,0))
                self.displayPoints()
                if self.current_figure[0] == "Circle": # Circle
                    #self.players[0].setDistance(math.sqrt((self.current_figure[1] - self.players[0].getX())**2 + (self.current_figure[2] - self.players[0].getY())**2) - self.current_figure[3])
                    #print math.sqrt((self.current_figure[1] - self.players[0].getX())**2 + (self.current_figure[2] - self.players[0].getY())**2) - self.current_figure[3]

                    self.displayCircle(self.current_figure[1], self.current_figure[2], self.current_figure[3])
                    self.screen.blit(self.match_surface, [0, self.height/10.0])
                    if  self.current_figure[3] + self.players[0].getRadius() > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        running = False
                        self.sentinel_time = False
                        self.winner0 = True
                        #self.time  = 0
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Voice: Collision Win
                        self.effect.play()
                        #time.sleep(2)
                        self.state = "ScoreSingleplayer"
                        #self.winner1 = True

                    ## Volume based on distance beetwen point and figure
                    elif self.current_figure[3] + self.players[0].getRadius() <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 50) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(1.0, 1.0)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 50) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 100) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.9, 0.9)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 100) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 150) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.8, 0.8)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 150) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 200) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.7, 0.7)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 200) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 250) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.6, 0.6)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 250) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 300) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.5, 0.5)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 300) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 350) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.4, 0.4)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 350) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 400) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.3, 0.3)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 400) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 450) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.2, 0.2)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 450) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 500) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.1, 0.1)

                    else:
                        self.multiplayer_channel0.set_volume(0.0, 0.0)

                else: # Rectangle
                    #self.players[0].setDistance(math.sqrt(((self.current_figure[1] + self.current_figure[3]/2)-self.players[0].getX())**2 + ((self.current_figure[2] + self.current_figure[3]/2)-self.players[0].getY())**2)  - min(self.current_figure[3], self.current_figure[4]))
                    #print math.sqrt(((self.current_figure[1] + self.current_figure[3]/2)-self.players[0].getX())**2 + ((self.current_figure[2] + self.current_figure[3]/2)-self.players[0].getY())**2) - min(self.current_figure[3], self.current_figure[4])

                    self.displayRectangle(self.current_figure[1], self.current_figure[2], self.current_figure[3], self.current_figure[4])
                    self.screen.blit(self.match_surface, [0, self.height/10.0])
                    if self.figure_rect.collidepoint(self.players[0].getX(), self.players[0].getY()):
                        running = False
                        self.sentinel_time = False
                        self.winner0 = True
                        #self.time  = 0
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Voice: Collision Win
                        self.effect.play()
                        self.state = "ScoreSingleplayer"

                    ## Volume based on distance beetwen point and figure
                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 50) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(1.0, 1.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 50 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 100) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.9, 0.9)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 100 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 150) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.8, 0.8)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 150 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 200) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.7, 0.7)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 200 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 250) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.6, 0.6)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 250 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 300) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.5, 0.5)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 300 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 350) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.4, 0.4)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 350 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 400) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.3, 0.3)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 400 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 450) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.2, 0.2)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 450 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 500) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.1, 0.1)

                    else:
                        self.multiplayer_channel0.set_volume(0.0, 0.0)

            #print pygame.mixer.music.get_volume()
            pygame.display.flip()
            pygame.display.update()

    def initMultiplayerMatch(self):
        time_surface = pygame.Surface((self.width, self.height-(self.height-(self.height/10))))
        running = True

        self.thread_time = Thread(target = self.matchTime, args = ([])) # Thread time configuration
        self.time = 60
        self.sentinel_time = True
        self.thread_time.start()

        while(running):
            self.clock.tick(30)

            if self.time == 0: # Max macth time
                running = False
                self.state = "ScoreMultiplayer"
                self.sentinel_time = False
                self.winner0 = False
                self.winner0 = False
                self.time  = 60

            time_surface.fill((0,0,0)) # Set time in screen surface
            self.screen.blit(time_surface, [0, 0])

            text = self.text_time.render("Time: "+ str(self.time), 1, (255, 255, 255))
            self.screen.blit(text, (300, 2))

            for event in pygame.event.get():
                #if event.type == pygame.QUIT: # exit event
                 #   self.running = False
                  #  running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT and self.players[0].getX() <= 5:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_LEFT:
                        self.players[0].moveLeft(10)

                    elif event.key == pygame.K_RIGHT and self.players[0].getX() >= self.width-10:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_RIGHT:
                        self.players[0].moveRight(10)

                    elif event.key == pygame.K_UP and self.players[0].getY() <= (self.height/10):
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_UP:
                        self.players[0].moveUp(10)

                    elif event.key == pygame.K_DOWN and self.players[0].getY() >= self.height-55:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_DOWN:
                        self.players[0].moveDown(10)

                    ## Event to player 2
                    if event.key == pygame.K_a and self.players[1].getX() <= 5:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_a:
                        self.players[1].moveLeft(10)

                    elif event.key == pygame.K_d and self.players[1].getX() >= self.width-10:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_d:
                        self.players[1].moveRight(10)

                    elif event.key == pygame.K_w and self.players[1].getY() <= (self.height/10):
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_w:
                        self.players[1].moveUp(10)

                    elif event.key == pygame.K_s and self.players[1].getY() >= self.height-55:
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Collision sound
                        self.effect.play()

                    elif event.key == pygame.K_s:
                        self.players[1].moveDown(10)

                self.match_surface.fill((0,0,0))
                self.displayPoints()
                if self.current_figure[0] == "Circle":
                    self.displayCircle(self.current_figure[1], self.current_figure[2], self.current_figure[3])
                    self.screen.blit(self.match_surface, [0, self.height/10.0])

                    ## player 0
                    if  self.current_figure[3] + self.players[0].getRadius() > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        running = False
                        self.sentinel_time = False
                        #self.time  = 0
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Voice: Collision Win
                        self.effect.play()
                        self.winner0 = True
                        self.winner1 = False
                        self.state = "ScoreMultiplayer"
                        self.multiplayer_channel0.stop()
                        self.multiplayer_channel1.stop()

                    ## Volume based on distance beetwen point and figure
                    elif self.current_figure[3] + self.players[0].getRadius() <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 50) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 1.0)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 50) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 100) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.9)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 100) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 150) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.8)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 150) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 200) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.7)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 200) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 250) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.6)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 250) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 300) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.5)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 300) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 350) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.4)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 350) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 400) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.3)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 400) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 450) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.2)

                    elif (self.current_figure[3] + self.players[0].getRadius() + 450) <= math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()) and (self.current_figure[3] + self.players[0].getRadius() + 500) > math.hypot(self.current_figure[1] - self.players[0].getX(), self.current_figure[2] - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.1)

                    else:
                        self.multiplayer_channel0.set_volume(0.0, 0.0)

                    ## player 1
                    if  self.current_figure[3] + self.players[1].getRadius() > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        running = False
                        self.sentinel_time = False
                        #self.time  = 0
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Voice: Collision Win
                        self.effect.play()
                        self.winner0 = False
                        self.winner1 = True
                        self.state = "ScoreMultiplayer"
                        self.multiplayer_channel0.stop()
                        self.multiplayer_channel1.stop()

                    ## Volume based on distance beetwen point and figure
                    elif self.current_figure[3] + self.players[1].getRadius() <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 50) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(1.0, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 50) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 100) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.9, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 100) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 150) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.8, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 150) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 200) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.7, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 200) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 250) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.6, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 250) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 300) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.5, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 300) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 350) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.4, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 350) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 400) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.3, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 400) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 450) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.2, 0.0)

                    elif (self.current_figure[3] + self.players[1].getRadius() + 450) <= math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()) and (self.current_figure[3] + self.players[1].getRadius() + 500) > math.hypot(self.current_figure[1] - self.players[1].getX(), self.current_figure[2] - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.1, 0.0)

                    else:
                        self.multiplayer_channel1.set_volume(0.0, 0.0)

                else:
                    self.displayRectangle(self.current_figure[1], self.current_figure[2], self.current_figure[3], self.current_figure[4])
                    self.screen.blit(self.match_surface, [0, self.height/10.0])

                    ## Player 0
                    if self.figure_rect.collidepoint(self.players[0].getX(), self.players[0].getY()):
                        running = False
                        self.sentinel_time = False
                        #self.time  = 0
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Voice: Collision Win
                        self.effect.play()
                        self.winner0 = True
                        self.winner1 = False
                        self.state = "ScoreMultiplayer"
                        self.multiplayer_channel0.stop()
                        self.multiplayer_channel1.stop()
                        #self.multiplayer_channel1.stop()

                    ## Volume based on distance beetwen point and figure
                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 50) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 1.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 50 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 100) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.9)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 100 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 150) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.8)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 150 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 200) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.7)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 200 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 250) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.6)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 250 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 300) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.5)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 300 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 350) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.4)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 350 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 400) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.3)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 400 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 450) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.2)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 450 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[0].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[0].getRadius() + 500) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[0].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[0].getY()):
                        self.multiplayer_channel0.set_volume(0.0, 0.1)

                    else:
                        self.multiplayer_channel0.set_volume(0.0, 0.0)

                    ## Player 1
                    if self.figure_rect.collidepoint(self.players[1].getX(), self.players[1].getY()):
                        running = False
                        self.sentinel_time = False
                        #self.time  = 0
                        self.effect = pygame.mixer.Sound("Audios/Intro/Example.ogg") # Voice: Collision Win
                        self.effect.play()
                        self.winner0 = False
                        self.winner1 = True
                        self.state = "ScoreMultiplayer"
                        self.multiplayer_channel0.stop()
                        self.multiplayer_channel1.stop()

                    ## Volume based on distance beetwen point and figure
                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 50) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(1.0, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 50 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 100) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.9, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 100 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 150) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.8, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 150 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 200) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.7, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 200 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 250) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.6, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 250 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 300) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.5, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 300 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 350) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.4, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 350 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 400) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.3, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 400 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 450) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.2, 0.0)

                    elif min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 450 <= math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), self.current_figure[2] + (self.current_figure[4]/3) - self.players[1].getY()) and (min(self.current_figure[3], self.current_figure[4])/3 + self.players[1].getRadius() + 500) > math.hypot((self.current_figure[1]+self.current_figure[3]/3) - self.players[1].getX(), (self.current_figure[2]+self.current_figure[4]/3) - self.players[1].getY()):
                        self.multiplayer_channel1.set_volume(0.1, 0.0)

                    else:

                        self.multiplayer_channel1.set_volume(0.0, 0.0)

            #print pygame.mixer.music.get_volume()
            pygame.display.flip()
            pygame.display.update()

    def createPoints(self, n=1): ## Verificate this
        self.players = [] # clear players list
        for i in range(0, n):
            rand_x = random.randint(1, self.width)
            rand_y = random.randint(self.height/10, self.height-50)
            while self.current_figure == "Circle" and self.current_figure[3] + 5 > math.hypot(self.current_figure[1] - rand_x, self.current_figure[2] - rand_y):
                rand_x = random.randint(1, self.width)
                rand_y = random.randint(self.height/10, self.height-50)

            while self.current_figure == "Rectangle" and self.figure_rect.collidepoint(rand_x, rand_y):
                rand_x = random.randint(1, self.width)
                rand_y = random.randint(self.height/10, self.height-50)

            self.players.append(Player(self.match_surface, (rand_x, rand_y), (0,0, 255), 5))

    def displayPoints(self):
        for player in self.players:
            player.display()

    def displayRectangle(self, x, y, r1, r2):
        self.figure_rect = pygame.Rect(x, y, r1, r2)
        pygame.draw.rect(self.match_surface, self.colors['red'], self.figure_rect)

    def displayCircle(self, x, y, radio):
        pygame.draw.circle(self.match_surface, self.colors['red'], (x, y), radio)

    def generateRandomFigure(self):
        rand = random.randint(0, 1)
        rand_x = random.randint(0, self.width)
        rand_y = random.randint(self.height/10, self.height-50)
        if rand == 0: # circle
            rand_r = random.randint(5, self.height/3)
            return ("Circle", rand_x, rand_y, rand_r)
        elif rand == 1: # Rectangle
            rand_y = random.randint(self.height/10, self.height-50)
            rand_x = random.randint(0, self.width-5)
            rand_r1 = random.randint(15, self.height/3) ## Verificate this
            rand_r2 = random.randint(15, self.height/3)
            return ("Rectangle", rand_x, rand_y, rand_r1, rand_r2)

    def scoreSingleplayer(self):
        running = True
        self.multiplayer_channel0.stop()
        if self.winner0 == True: # Winner!!!!
            sound = pygame.mixer.Sound("Audios/Score/WinnerScore_mighty_like_us.ogg")
            self.multiplayer_channel0 = sound.play()
            self.multiplayer_channel0.set_volume(5.0, 5.0)
        else:
            sound = pygame.mixer.Sound("Audios/Score/commonScore_level_up.mp3")
            self.multiplayer_channel0 = sound.play()
            self.multiplayer_channel0.set_volume(0.5, 0.5)

        time = self.time

        while(running):
            self.clock.tick(30)

            initial_surface = pygame.Surface((self.width, self.height))
            initial_surface.fill((0,0,0))
            self.screen.blit(initial_surface, [0, 0])

            text_initial = pygame.font.SysFont(self.font_default, self.height/5)
            text = text_initial.render("Scores", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3.0, 30))

            text_initial_subscript = pygame.font.SysFont(self.font_default, self.height/10)
            text = text_initial_subscript .render("Time: " + str(time+1), 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3, 145))

            #text = text_initial_subscript.render("Distance: " + str(self.players[0].getDistance()) , 1, (255, 255, 255))
            #self.screen.blit(text, (self.width/3, 180))

            text_initial_menu = pygame.font.SysFont(self.font_default, self.height/10)
            text = text_initial_menu.render("Jogar novamente:", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3.0, 280))

            text_initial_menu = pygame.font.SysFont(self.font_default, self.height/12)
            text = text_initial_menu.render("1 - Yes", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.4, 320))

            text = text_initial_menu.render("2 - No", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.4, 360))

            text = text_initial_menu.render("=>", 1, (255, 255, 255)) # Arrow. change to a arrow firure aften.

            if self.option_initial == 1:
                self.screen.blit(text, ((self.width/2.4)-35, 320 - 2))
            if self.option_initial == 2:
                self.screen.blit(text, ((self.width/2.4)-35, 360 - 2))

            for event in pygame.event.get():
                #if event.type == pygame.QUIT: # exit event
                 #   self.running = False
                  #  running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.option_initial = 1
                        pygame.mixer.music.load("Audios/Voice/Sim.mp3")
                        pygame.mixer.music.play()

                    if event.key == pygame.K_DOWN:
                        self.option_initial = 2
                        #self.effect.stop()
                        pygame.mixer.music.load("Audios/Voice/Nao.mp3")
                        pygame.mixer.music.play()

                    if event.key == pygame.K_RETURN and self.option_initial == 1:
                        self.current_figure = self.generateRandomFigure()
                        #pygame.mixer.music.stop()
                        self.effect.stop()
                        self.multiplayer_channel0.stop()
                        #pygame.mixer.music.stop()
                        running = False # leave this while"
                        self.state = "Singleplayer"
                        self.screen.fill((0,0,0)) # Set black in screen background
                        self.createPoints() # Init points of players

                    if event.key == pygame.K_RETURN and self.option_initial == 2:
                        #self.current_figure = self.generateRandomFigure()
                        #pygame.mixer.music.stop()
                        self.effect.stop()
                        self.multiplayer_channel0.stop()
                        #pygame.mixer.music.stop()
                        running = False # leave this while
                        self.state = "InitialMenu"
                        self.screen.fill((0,0,0)) # Set black in screen background
                        #self.createPoints(n=2) # Init points of players

            pygame.display.flip()
            pygame.display.update()

    def scoreMultiplayer(self):
        running = True
        if self.winner0 == True: # Winner!!!!
            sound = pygame.mixer.Sound("Audios/Score/WinnerScore_mighty_like_us.ogg")
            self.multiplayer_channel0 = sound.play()
            self.multiplayer_channel0.set_volume(0.0, 1.0)
        else:
            sound = pygame.mixer.Sound("Audios/Score/commonScore_level_up.ogg")
            self.multiplayer_channel0 = sound.play()
            self.multiplayer_channel0.set_volume(0.0, 1.0)

        if self.winner1 == True:
            sound = pygame.mixer.Sound("Audios/Score/WinnerScore_mighty_like_us.ogg")
            self.multiplayer_channel1 = sound.play()
            self.multiplayer_channel1.set_volume(1.0, 0.0)
        else:
            sound = pygame.mixer.Sound("Audios/Score/commonScore_level_up.ogg")
            self.multiplayer_channel1 = sound.play()
            self.multiplayer_channel1.set_volume(1.0, 0.0)

        time = self.time

        while(running):
            self.clock.tick(30)

            initial_surface = pygame.Surface((self.width, self.height))
            initial_surface.fill((0,0,0))
            self.screen.blit(initial_surface, [0, 0])

            text_initial = pygame.font.SysFont(self.font_default, self.height/5)
            text = text_initial.render("Scores", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3.0, 30))

            text_initial_subscript = pygame.font.SysFont(self.font_default, self.height/10)
            text = text_initial_subscript .render("Time: " + str(time+1), 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3, 145))

            #text = text_initial_subscript.render("Distance: " + str(self.players[0].getDistance()) , 1, (255, 255, 255))
            #self.screen.blit(text, (self.width/3, 180))

            text_initial_menu = pygame.font.SysFont(self.font_default, self.height/10)
            text = text_initial_menu.render("Jogar novamente:", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/3.0, 280))

            text_initial_menu = pygame.font.SysFont(self.font_default, self.height/12)
            text = text_initial_menu.render("1 - Yes", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.4, 320))

            text = text_initial_menu.render("2 - No", 1, (255, 255, 255))
            self.screen.blit(text, (self.width/2.4, 360))

            text = text_initial_menu.render("=>", 1, (255, 255, 255)) # Arrow. change to a arrow firure aften.

            if self.option_initial == 1:
                self.screen.blit(text, ((self.width/2.4)-35, 320 - 2))
            if self.option_initial == 2:
                self.screen.blit(text, ((self.width/2.4)-35, 360 - 2))

            for event in pygame.event.get():
                #if event.type == pygame.QUIT: # exit event
                 #   self.running = False
                  #  running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.option_initial = 1
                        pygame.mixer.music.load("Audios/Voice/Sim.mp3")
                        pygame.mixer.music.play()
                        #time.sleep(1)

                    if event.key == pygame.K_DOWN:
                        self.option_initial = 2
                        #self.effect.stop()
                        pygame.mixer.music.load("Audios/Voice/Nao.mp3")
                        pygame.mixer.music.play()
                        #time.sleep(1)

                    if event.key == pygame.K_RETURN and self.option_initial == 1:
                        self.current_figure = self.generateRandomFigure()
                        #pygame.mixer.music.stop()
                        self.effect.stop()
                        running = False # leave this while"
                        self.state = "Multiplayer"
                        self.multiplayer_channel0.stop()
                        self.multiplayer_channel1.stop()
                        self.screen.fill((0,0,0)) # Set black in screen background
                        self.createPoints(n=2) # Init points of players
                        self.winner0 = True
                        self.winner1 = True

                    if event.key == pygame.K_RETURN and self.option_initial == 2:
                        #self.current_figure = self.generateRandomFigure()
                        #pygame.mixer.music.stop()
                        self.effect.stop()
                        self.multiplayer_channel0.stop()
                        self.multiplayer_channel1.stop()
                        running = False # leave this while
                        self.state = "InitialMenu"
                        self.screen.fill((0,0,0)) # Set black in screen background
                        #self.createPoints(n=2) # Init points of players

            pygame.display.flip()
            pygame.display.update()