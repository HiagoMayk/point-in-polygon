
import pygame as pg
import time

class AudioManeger:

    def __init__(self):
        self.intro_audios = ["intro_kings_of_tara", "intro_nowhere_tara", "musica"]
        self.match_audio = ["match_floating_cities", "match_vanishing"]
        self.score_audio = ["commonScore_level_up", "WinnerScore_mighty_like_us"]
        self.effect_audio = ["applause1", "applause2", "applause3", "applause4", "applause5", "golpe1", "cogiendofuerza", "golpepunto", "oh"]
        self.voice_audio = ["Aventura", "Bem_vindo", "boa_sorte", "Como_quer_jogar", "fim_de_jogo", "ganhou", "jogar_novamente", "jogo_ja_vai_comecar", "modo_de_jogo", "objetivo_alcancado", "objetivo_nao_alcancado", "parabens", "preparese", "sua_classificacao_e"]

    def play_music(self, music_file, volume=0.8):
        # set up the mixer
        freq = 44100     # audio CD quality
        bitsize = -16    # unsigned 16 bit
        channels = 2     # 1 is mono, 2 is stereo
        buffer = 2048    # number of samples (experiment to get best sound)
        pg.mixer.init(freq, bitsize, channels, buffer)
        # volume value 0.0 to 1.0
        pg.mixer.music.set_volume(volume)
        clock = pg.time.Clock()
        try:    
            pg.mixer.music.load(music_file)
            print("Music file {} loaded!".format(music_file))
        except pg.error:
            print("File {} not found! ({})".format(music_file, pg.get_error()))
            return
        pg.mixer.music.play(-1)
        time.sleep(10)  
        while pg.mixer.music.get_busy():
            # check if playback has finished
            pg.mixer.music.load("Audios/Voice/Bem_vindo.mp3")
            pg.mixer.music.play()
            time.sleep(10)
            clock.tick(30)  

    
