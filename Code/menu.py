
from AudioManeger import *
from threading import Thread
from time import sleep

class Menu:

    def __init__(self):
        self.audio_maneger = AudioManeger()

    def introMenu(self):
        thread1 = Thread(target = self.audio_maneger.play_music, args = ("Audios/Intro/intro_kings_of_tara.mp3",0.5))
        thread2 = Thread(target = self.audio_maneger.play_music, args = ("Audios/Voice/Bem_vindo.mp3",0.8))

        thread2.start()
        thread1.start()
        thread1.join()
        thread2.join()
        
        print "thread finished...exiting"
